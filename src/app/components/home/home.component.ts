import { Component, OnInit, OnDestroy } from '@angular/core';
import { GridsSandbox } from 'src/app/sandboxes/grid.sandbox';
import { Observable, Subscription } from 'rxjs';
import { IGrid } from 'src/app/model/grid.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  gridsSubscription: Subscription;
  grids$: Observable<IGrid[]>;

  constructor(private gridSandbox: GridsSandbox) {

  }

  setActiveGrid(grid: IGrid) {
    this.gridSandbox.setActiveGrid(grid);
  }

  ngOnInit() {
    this.grids$ = this.gridSandbox.grids$;

    this.gridsSubscription = this.grids$.subscribe(grids => {
      console.log('Grids here!');
    });

    this.gridSandbox.fetchGrids();
  }

  ngOnDestroy(): void {
    this.gridsSubscription.unsubscribe();
  }

}
