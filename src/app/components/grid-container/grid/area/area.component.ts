import { Component, OnInit, Input } from '@angular/core';
import { IArea } from 'src/app/model/area.model';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {

  constructor() { }

  @Input() area: IArea;
  @Input() offsetTop: number;
  @Input() offsetLeft: number;
  @Input() columnWidth: number;
  @Input() rowHeight: number;

  ngOnInit() {
  }

}
