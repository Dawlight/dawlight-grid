// import { Subscription } from 'rxjs';
import { AreasService } from 'src/app/services/areas.service';
import { OnInit, OnDestroy, Input, Component, HostListener, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { GridsSandbox } from 'src/app/sandboxes/grid.sandbox';
import { IGrid } from 'src/app/model/grid.model';
import { ElementRef } from '@angular/core';
import { Observable, Subscription, Subject } from 'rxjs';

interface IGridCoordinate {
  x: number;
  y: number;
}

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, OnDestroy {

  horizontalGridData = { left: 0, top: 0, width: 0, height: 0 };
  verticalGridData = { left: 0, top: 0, width: 0, height: 0 };

  containerWidth = 0;
  containerHeight = 0;

  containerSizeSubscription: Subscription;
  mouseOver: boolean;

  constructor(private element: ElementRef, private gridsSandbox: GridsSandbox) { }

  @Input() containerSize: Observable<number[]>;
  @Input() grid: IGrid;

  ngOnInit() {
    this.containerSize.subscribe(size => {
      this.updateContainerSize(size[0], size[1]);
    });
  }

  @HostListener('mouseenter', ['$event'])
  onMouseEnter() {

  }

  @HostListener('mouseleave', ['$event'])
  onMouseLeave() {
    this.mouseOver = false;
  }

  @HostListener('mousemove', ['$event'])
  onMouseOver($event: MouseEvent) {
    this.mouseOver = true;
    if ($event.target === this.element.nativeElement) {

      const focusedGridCoordinate = this.getMouseGridCoordinates($event.offsetX, $event.offsetY);

      this.horizontalGridData = {
        left: 0,
        top: this.getCellHeight() * focusedGridCoordinate.y,
        width: this.containerWidth,
        height: this.getCellHeight()
      };

      this.verticalGridData = {
        left: this.getCellWidth() * focusedGridCoordinate.x,
        top: 0,
        width: this.getCellWidth(),
        height: this.containerHeight
      };

    }
    // console.log(`x: ${this.focusedGridData.top}, y: ${this.focusedGridData.left}`);
  }

  private getMouseGridCoordinates(elementX: number, elementY: number) {
    if (!this.grid)
      return { x: 0, y: 0 };

    const columnNumber = Math.floor(elementX / this.getCellWidth());
    const rowNumber = Math.floor(elementY / this.getCellHeight());

    console.log(`x: ${elementX}, y: ${elementY}`);
    console.log(`coln: ${columnNumber}, rown: ${rowNumber}`);

    return { x: columnNumber, y: rowNumber };
  }

  public updateContainerSize(width: number, height: number) {
    this.containerWidth = width;
    this.containerHeight = height;
  }

  public getCellWidth() {
    if (this.grid)
      return this.containerWidth / this.grid.columns;
    return 0;
  }

  public getCellHeight() {
    if (this.grid)
      return this.containerHeight / this.grid.rows;
    return 0;
  }

  ngOnDestroy() {
    this.containerSizeSubscription.unsubscribe();
  }
}
