import { Component, OnInit, HostListener, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { GridsSandbox } from 'src/app/sandboxes/grid.sandbox';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { IGrid } from 'src/app/model/grid.model';
import { GridComponent } from './grid/grid.component';

@Component({
  selector: 'app-grid-container',
  templateUrl: './grid-container.component.html',
  styleUrls: ['./grid-container.component.scss']
})
export class GridContainerComponent implements OnInit, OnDestroy {
  grid$: Observable<IGrid>;
  containerSize$: Observable<number[]>;
  containerSizeSubject: BehaviorSubject<number[]>;

  constructor(private element: ElementRef, private gridsSandbox: GridsSandbox) { }

  @HostListener('window:resize')
  onResize() {
    this.containerSizeSubject.next(this.getElementSize());
  }

  ngOnInit() {
    this.containerSizeSubject = new BehaviorSubject<number[]>(this.getElementSize());
    this.grid$ = this.gridsSandbox.activeGrid$;
    this.containerSize$ = this.containerSizeSubject.asObservable();
  }

  getElementSize() {
    return [this.element.nativeElement.offsetWidth, this.element.nativeElement.offsetHeight];
  }

  ngOnDestroy() {
    this.containerSizeSubject.complete();
  }

}
