import { Store } from '@ngxs/store';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'dawlight-grid';

  constructor(private store: Store) { }

  ngOnDestroy(): void {
  }
  ngOnInit(): void {
  }

}
