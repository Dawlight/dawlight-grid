import { AddGrid, SetActiveGrid } from './../store/grids/grids.actions';
import { IGrid } from './../model/grid.model';
import { Observable, from } from 'rxjs';
import { map, flatMap, tap, switchMap } from 'rxjs/operators';
import { AreasService, IAreaData } from './../services/areas.service';
import { GridsService, IGridData } from './../services/grids.service';
import { Injectable } from '@angular/core';
import { Store } from '@ngxs/store';
import { IAppState } from '../store/app.state';
import { IArea } from '../model/area.model';

@Injectable({
  providedIn: 'root'
})
export class GridsSandbox {

  public grids$: Observable<IGrid[]>;
  public activeGrid$: Observable<IGrid>;

  constructor(private store: Store, private gridsService: GridsService, private areasService: AreasService) {
    this.grids$ = this.store.select((state: IAppState) => state.grids.grids);
    this.activeGrid$ = this.store.select((state: IAppState) => state.grids.activeGrid);
  }

  fetchGrids() {
    this.gridsService.getAll()
      .pipe(switchMap(grids => from(grids)))
      .pipe(map(gridData => this.mapGridData(gridData)))
      .pipe(tap<IGrid>(grid => this.populateAndAddGrid(grid)))
      .subscribe();
  }

  addGrid(grid: IGrid) {
    this.store.dispatch(new AddGrid(grid));
  }


  setActiveGrid(grid: IGrid) {
    this.store.dispatch(new SetActiveGrid(grid));
  }


  private mapGridData(gridData: IGridData) {
    return <IGrid>{
      id: gridData.id,
      columns: gridData.columns,
      rows: gridData.rows,
      areas: []
    };
  }

  private populateAndAddGrid(grid: IGrid) {
    this.areasService.getAreasByGridId(grid.id)
      .pipe(tap(areas => console.log(areas)))
      .pipe(map(areas => areas.map(areaData => this.mapAreaData(areaData))))
      .pipe(tap(areas => {
        grid.areas = areas;
        this.addGrid(grid);
      })).subscribe();
  }

  private mapAreaData(areaData: IAreaData) {
    return <IArea>{
      id: areaData.id,
      width: areaData.width,
      height: areaData.height,
      top: areaData.top,
      left: areaData.left,
    };
  }
}

