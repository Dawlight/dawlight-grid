import { HttpClient } from '@angular/common/http';
import { Observable, Subscriber } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

export interface IAreaData {
  id: number;
  gridId: number;
  width: number;
  height: number;
  top: number;
  left: number;
}

@Injectable({
  providedIn: 'root'
})
export class AreasService {
  constructor(private httpClient: HttpClient) { }

  getArea(areaId: number) {
    if (!environment.production)
      return this.httpClient.get<IAreaData>(`http://localhost:3000/areas/${areaId}`);


    return new Observable<IAreaData>(subscriber => {
      subscriber.next({ width: 0, height: 0, top: 0, left: 0, id: 0, gridId: 0 });
    });
  }

  getAreasByGridId(gridId: number) {
    if (!environment.production)
      return this.httpClient.get<IAreaData[]>(`http://localhost:3000/grids/${gridId}/areas`);

    return new Observable<IAreaData[]>(subscriber => {
      subscriber.next([]);
    });
  }

  getAll() {
    if (!environment.production)
      return this.httpClient.get<IAreaData[]>(`http://localhost:3000/areas`);


    return new Observable<IAreaData[]>(subscriber => {
      subscriber.next([]);
    });
  }
}
