import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export interface IGridData {
  id: number;
  columns: number;
  rows: number;
}

@Injectable({
  providedIn: 'root'
})
export class GridsService {

  constructor(private httpClient: HttpClient) { }

  public getGrid(id: string) {
    if (!environment.production)
      return this.httpClient.get<IGridData>(`http://localhost:3000/grids/${id}`);

    return new Observable<IGridData>(subscriber => {
      subscriber.next({ columns: 0, rows: 0, id: 0 });
    });
  }

  public getAll() {
    if (!environment.production)
      return this.httpClient.get<IGridData[]>(`http://localhost:3000/grids`);

    return new Observable<IGridData[]>(subscriber => {
      subscriber.next([]);
    });
  }

  public addGrid(id: string, columns: number, rows: number) {
    if (!environment.production)
      return this.httpClient.post<IGridData[]>(`http://localhost:3000/grids`, { id, columns, rows });

    return new Observable<IGridData[]>(subscriber => {
      subscriber.next([]);
    });
  }

}

