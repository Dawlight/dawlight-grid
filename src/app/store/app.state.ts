import { RouterStateModel } from '@ngxs/router-plugin';
import { GridsState } from './grids/grids.state';
import { GridsStateModel } from './grids/grids.model';


export interface IAppState {
  router: RouterStateModel;
  grids: GridsStateModel;
}

export const appState = [
  GridsState
];
