import { IGrid } from 'src/app/model/grid.model';

export class AddGrid {
  static type = '[Grids] add grid';
  constructor(public grid: IGrid) { }
}

export class SetActiveGrid {
  static type = '[Grids] set active grid';
  constructor(public grid: IGrid) {}
}
