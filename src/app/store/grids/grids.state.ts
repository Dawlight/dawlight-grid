import { AddGrid, SetActiveGrid } from './grids.actions';
import { GridsStateModel } from './grids.model';
import { State, Action, StateContext, Selector } from '@ngxs/store';

@State<GridsStateModel>({
  name: 'grids',
  defaults: {
    activeGrid: null,
    grids: []
  }
})
export class GridsState {

  @Action(AddGrid)
  addGrid(context: StateContext<GridsStateModel>, action: AddGrid) {
    const state = context.getState();

    context.setState({
      ...state,
      grids: [...state.grids, action.grid]
    });
  }

  @Action(SetActiveGrid)
  setActiveGrid(context: StateContext<GridsStateModel>, action: SetActiveGrid) {
    const state = context.getState();

    context.setState({
      ...state,
      activeGrid: action.grid
    });
  }

}
