import { IGrid } from './../../model/grid.model';

export class GridsStateModel {
  grids: IGrid[];
  activeGrid: IGrid;
}

