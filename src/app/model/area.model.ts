export interface IArea {
  id: number;
  width: number;
  height: number;
  top: number;
  left: number;
}
