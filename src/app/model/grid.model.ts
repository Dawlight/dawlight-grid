import { IArea } from './area.model';

export interface IGrid {
  id: number;
  columns: number;
  rows: number;
  areas: IArea[];
}
